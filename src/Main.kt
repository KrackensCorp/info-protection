import anonimys.ReceptionOfVotes

@ExperimentalUnsignedTypes
fun main() {
    /*  1lab */
    /*
    val library = LibraryManager()
    library.powsRandom()
    library.extendedEuclideanRandom()
    library.hellmanRandom()
    library.babyStepGiantStepRandom()
    */

    /* 2 lab */
    /*
    val libraly.contract.data = FileUtils.fileToLongList("1.png")
    val method = ShamirCipher()             // Шифр Шамира
    //var method = ElGamaliaCipher()          // Шифр Эль-Гамаля
    //var method = VernamCipher()             // Шифр Вернама
    //var method = RsaCipher()                // Шифр RSA

    val library = EncryptionLibrary(method)
    val encrypt = library.encryptAllMessage(libraly.contract.data)
    FileUtils.longListToFile("encrypt.png", encrypt)
    val result = library.decryptAllMessage(encrypt)
    FileUtils.longListToFile("decrypt.png", result)
    */

    /* 3 lab */
    /*
    val data = FileUtils.fileToByteArray("1.png")
//    val method = ElGamaliaCipher()
//    val method = RsaCipher()
    val method = GostElectronicSignature()
    val signLib = SignatureLibrary(method)
    val hash = signLib.singAllMessage(data.toList())
    if (signLib.verifyAllMessage(hash)){
        println("Verification  was successful!")
    } else {
        println("Verification error")
    }
    */

    /* 4 lab */
    /*
    MentalPoker().distributionCards(2)
    */

    /* 5 lab */
    ReceptionOfVotes(60).holdVote()
}
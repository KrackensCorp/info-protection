package utils

import com.google.common.hash.Hashing


object HashUtils {
    fun sha(str: Long): Long {
        return Hashing.sha512()
            .hashLong(str)
            .asLong()

    }
}
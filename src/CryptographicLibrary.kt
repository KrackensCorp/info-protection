import kotlin.math.pow


@ExperimentalUnsignedTypes
class CryptographicLibrary {
    /** Функция быстрого возведения числа [a1] в степень [b1] по модулю [m] */
    fun pows(a1: Long, b1: Long, m: Long): Long {
        var a: Long = a1
        var b: Long = b1
        var res = 1L
        a %= m
        while (b > 0L) {
            if (b and 1L == 1L)
                res = res * a % m
            b = b shr 1
            a = a * a % m
        }
        return res
    }

    /** Функция быстрого возведения числа [a1] в степень [b1] c множетелем [x] по модулю [m] */
    fun pows(a1: Long, b1: Long, m: Long, x: Long): Long {
        return (pows(a1, b1, m) * x % m) % m
    }

    fun isPrime(p: Int): Boolean {
        if (p <= 1) return false
        val b = p.toDouble().pow(0.5).toInt()
        for (i in 2..b) {
            if (p % i == 0) return false
        }
        return true
    }

    /** Алгоритм Евклида нахождения НОД двух чисел*/
    fun euclidean(a: Long, b: Long): Long {
        return when (b) {
            0L -> a
            else -> euclidean(b, a % b)
        }
    }

    /** Расширенный (обобщенный) алгоритм Евклида [ax + by = gcd(a,b)] */
    fun extendedEuclidean(a: Long, b: Long): GcdExtData {
        return if (b > a) gcdExt(b, a) else gcdExt(a, b)
    }

    /** Результаты расчета расширенного алгоритма Евклида */
    class GcdExtData(var d: Long, var x: Long, var y: Long)

    private fun gcdExt(a: Long, b: Long): GcdExtData {
        if (b == 0L) {
            return GcdExtData(a, 1L, 0L)
        }
        val res = gcdExt(b, a % b)
        val s = res.y
        res.y = res.x - (a / b) * res.y
        res.x = s
        return res
    }
}